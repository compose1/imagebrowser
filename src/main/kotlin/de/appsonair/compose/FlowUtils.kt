package de.appsonair.compose

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import log

/**
 * Executes loadPage until result list is empty and emits the items as flow.
 */
fun <T>pagingFlow(firstPage: Int = 0, maxPages: Int = 100, init: (suspend FlowCollector<T>.() -> Unit)? = null, loadPage: suspend (Int) -> List<T>): Flow<T> {
    var page = firstPage
    return flow {
        init?.invoke(this)
        var emittedValues = true
        while (page < maxPages && emittedValues) {
            emittedValues = false
            loadPage(page).forEach {
                emit(it)
                emittedValues = true
            }
            page++
        }
    }
}


fun <T> Flow<T>.chunked(size: Int) = flow<List<T>> {
    var buffer = ArrayList<T>(size)
    collect { item ->
        buffer.add(item)
        if (buffer.size >= size) {
            emit(buffer)
            buffer = ArrayList(size)
        }
    }
    if (buffer.size > 0) {
        emit(buffer)
    }
}

@Composable
fun <T> FlowColumnFor(
    flow2StateList: FlowListCollector<T>,
    modifier: Modifier = Modifier,
    state: LazyListState = rememberLazyListState(),
    contentPadding: PaddingValues = PaddingValues(0.dp),
    horizontalAlignment: Alignment.Horizontal = Alignment.Start,
    itemContent: @Composable LazyItemScope.(FlowState<T>) -> Unit
) {
    val buffer = 10
    LazyColumn(
        modifier = modifier,
        state = state,
        contentPadding = contentPadding,
        horizontalAlignment = horizontalAlignment
    ) {
        itemsIndexed(flow2StateList.list){ index, item ->
            flow2StateList.requestListSize(index + 1 + buffer)
            itemContent(this, item)
        }
    }
}

@Composable
fun <T> FlowRowFor(
    flow2StateList: FlowListCollector<T>,
    modifier: Modifier = Modifier,
    state: LazyListState = rememberLazyListState(),
    contentPadding: PaddingValues = PaddingValues(0.dp),
    verticalAlignment: Alignment.Vertical = Alignment.Top,
    itemContent: @Composable LazyItemScope.(FlowState<T>) -> Unit
) {
    val buffer = 10
    LazyRow(
        modifier = modifier,
        state = state,
        contentPadding = contentPadding,
        verticalAlignment = verticalAlignment
    ) {
        itemsIndexed(flow2StateList.list) { index, item ->
            flow2StateList.requestListSize(index + 1 + buffer)
            itemContent(this, item)
        }
    }
}

@Composable
fun <T> FlowMultiColumnFor(columns: Int, stateList: FlowListCollector<T>, modifier: Modifier = Modifier, girdPadding: Dp = 0.dp, state: LazyListState = rememberLazyListState(), itemCallback: @Composable (FlowState<T>) -> Unit) {
    val buffer = 10
    LazyColumn(
        modifier = modifier,
        state = state,
        contentPadding = PaddingValues(girdPadding)
    ) {
        itemsIndexed(stateList.list.chunked(columns)) { index, rowList ->
            stateList.requestListSize((index + 1 + buffer) * columns)
            Row(Modifier.padding(top = girdPadding, start = girdPadding)) {
                val rowModifier = Modifier.weight(1f).padding(end = girdPadding).align(Alignment.CenterVertically)
                rowList.forEach {
                    Box(rowModifier) {
                        itemCallback(it)
                    }
                }
                val emptyRows = (columns - rowList.size)
                repeat(emptyRows) {
                    Spacer(modifier = rowModifier)
                }
            }
        }
    }
}

fun <T> Flow<T>.stateList() = FlowListCollector(this)

sealed class FlowState<out T> {
    object Loading : FlowState<Nothing>()
    object Empty : FlowState<Nothing>()
    class Error(val error: Throwable): FlowState<Nothing>()
    class Item<T>(val value: T): FlowState<T>()
}

class FlowListCollector<T>(
    private val flow: Flow<T>
): CoroutineScope {
    private val job = Job()
    override val coroutineContext = Dispatchers.Main + job
    val list: SnapshotStateList<FlowState<T>> = mutableStateListOf()

    private val listItemsChannel = Channel<Int>(Channel.CONFLATED)
    private val requestItemsChannel = Channel<Int>(Channel.CONFLATED)
    private val retryChannel = Channel<Boolean>(Channel.CONFLATED)
    private var requestedListSize = 1
    private var listSize = 0

    init {
        list.add(FlowState.Loading) // set last list item to loading
        launch {
            var inProgress = true
            log("$this - Start collecting for $flow")
            if (requestedListSize <= 0) {
                requestedListSize = requestItemsChannel.receive()
            }

            flow.buffer(5)
                .retryWhen { cause, _ ->
                    log("Retry", cause)
                    if (inProgress) {
                        list.removeLast()
                    }
                    list.add(FlowState.Error(cause)) // Add error item to end of list
                    listItemsChannel.send(list.size)
                    inProgress = true
                    return@retryWhen retryChannel.receive()
                }.collect {
                    if (inProgress) {
                        list.removeLast()
                        inProgress = false
                    }
                    list.add(FlowState.Item(it))
                    listSize++
                    listItemsChannel.send(list.size)
                    if (listSize >= requestedListSize) {
                        log("Wait until new items needed. List size: $listSize requested: $requestedListSize")
                        requestedListSize = requestItemsChannel.receive()
                    }
                }
            if (inProgress) {
                inProgress = false
                list.removeLast()
                if (list.isEmpty()) {
                    list.add(FlowState.Empty)
                }
            }
            log("$this - collection ready")
        }
    }

    suspend fun preload(requestSize: Int) {
        if (job.isActive) {
            requestItemsChannel.send(requestSize)
            //Wait until preload is ready
            while (listSize < requestSize) {
                val newSize = listItemsChannel.receive()
                log("new size: $newSize list size: ${listSize}")
            }
        }
    }

    fun requestListSize(requestSize: Int) {
        if (job.isActive) {
            launch {
                log("Request items. List size: $listSize requested: $requestSize")
                if (listSize < requestSize) {
                    log("Send request")
                    requestItemsChannel.send(requestSize)
                }
            }
        }
    }

    fun retry() {
        launch {
            retryChannel.send(true)
        }
    }

    protected fun finalize() {
        job.cancel()
        requestItemsChannel.cancel()
        retryChannel.cancel()
        log("$this - Channel and job canceled")
    }

}
