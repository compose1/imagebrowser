package de.appsonair.compose

import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.asSkiaBitmap
import androidx.compose.ui.graphics.toComposeImageBitmap
import com.drew.imaging.ImageMetadataReader
import com.drew.imaging.jpeg.JpegMetadataReader
import com.drew.imaging.jpeg.JpegSegmentType
import com.drew.metadata.Directory
import com.drew.metadata.Metadata
import com.drew.metadata.MetadataException
import com.drew.metadata.exif.ExifIFD0Directory
import com.drew.metadata.exif.ExifReader
import com.drew.metadata.exif.ExifThumbnailDirectory
import com.drew.metadata.jpeg.JpegDirectory
import com.drew.metadata.jpeg.JpegReader
import com.drew.metadata.png.PngDirectory
import com.drew.metadata.webp.WebpDirectory
import imagebrowser.RemoteImage
import kotlinx.coroutines.*
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import log
import org.jetbrains.skia.*
import uk.co.caprica.vlcj.factory.discovery.NativeDiscovery
import uk.co.caprica.vlcj.player.base.MediaPlayer
import uk.co.caprica.vlcj.player.base.MediaPlayerEventAdapter
import uk.co.caprica.vlcj.player.component.CallbackMediaPlayerComponent
import java.awt.Dimension
import java.io.File
import java.io.IOException
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import kotlin.math.roundToInt


val diskIOContext = newSingleThreadContext("DiskIOcontext")
val folderIOContext = newSingleThreadContext("FolderIOcontext")
val skiaContext = newFixedThreadPoolContext(2, "SkiaContext")

data class Folder(val file: File, val childFolderCount: Int, val childFileCount: Int)

/**
 * apps on air
 *
 * @author Timo Drick
 */

val jpegSizeCache: MutableMap<String, ByteArray> = mutableMapOf()

suspend fun loadFile(file: File) = withContext(diskIOContext) { file.readBytes() }
suspend fun decodeToImage(byteArray: ByteArray): Image = withContext(skiaContext) {
    Image.makeFromEncoded(byteArray)
}
suspend fun loadImageFile(file: File): Image = decodeToImage(loadFile(file))

fun getImageSizeFileCached(file: File, width: Int): ImageBitmap? {
    val key = "${file.absolutePath}:$width"
    val cachedData = jpegSizeCache[key]
    return if (cachedData != null) Image.makeFromEncoded(cachedData).toComposeImageBitmap() else null
}

val vlcMutex = Mutex()

suspend fun videoThumbnail(file: File): ImageBitmap = withContext(Dispatchers.Default) {
    vlcMutex.withLock {
        NativeDiscovery().discover()
        //val factory = MediaPlayerFactory(NativeDiscovery(), VLC_ARGS)
        //val mediaPlayer = factory.mediaPlayers().newEmbeddedMediaPlayer()
        val mediaPlayer = CallbackMediaPlayerComponent().mediaPlayer()
        val positionLock = CountDownLatch(1)
        mediaPlayer.events().addMediaPlayerEventListener(object : MediaPlayerEventAdapter() {
            override fun positionChanged(mediaPlayer: MediaPlayer?, newPosition: Float) {
                log("Position changed: $newPosition")
                positionLock.countDown()
            }
        })
        try {
            log("Start")
            if (mediaPlayer.media().start(file.toString())) {
                mediaPlayer.subpictures().setTrack(-1)
                mediaPlayer.audio().setTrack(-1)
                //val media = mediaPlayer.media().newMedia()
                log("set position")
                mediaPlayer.controls().setPosition(0.01f)
                log("wait for position")
                positionLock.await(1, TimeUnit.SECONDS)
                log("Save snapshot")
                mediaPlayer.snapshots().get().toComposeImageBitmap()
            } else {
                throw IOException("Unable to take snapshot!")
            }
        } finally {
            mediaPlayer.release()
            delay(200)
        }
    }
}

suspend fun loadImageSizeFileCached(file: File, width: Int): ImageBitmap = withContext(skiaContext) {
    val key = "${file.absolutePath}:$width"
    val cachedData = jpegSizeCache[key]
    if (cachedData == null) {
        //try to load thumbnail from file
        val image = if (file.isVideoFile()) {
            Image.makeFromBitmap(videoThumbnail(file).asSkiaBitmap())
        } else {
            val thumbnail = loadExifThumbnail(file)
            if (thumbnail != null) {
                val thumbImage = Image.makeFromEncoded(thumbnail.data)
                rotateUsingSurface(thumbImage, thumbnail.orientation)
            } else {
                log("${file.name} Creating thumbnail")
                val fullImage = Image.makeFromEncoded(loadFile(file))
                val ratio = fullImage.width.toFloat() / fullImage.height.toFloat()
                val height = (width.toFloat() / ratio).roundToInt()
                scaleUsingSurface(fullImage, width, height)
            }
        }
        // compress data to jpeg for caching
        val bytes = image.encodeToData(EncodedImageFormat.JPEG, 80)!!.bytes
        jpegSizeCache[key] = bytes
        image.toComposeImageBitmap()
    } else {
        Image.makeFromEncoded(cachedData).toComposeImageBitmap()
    }
}

fun scaleUsingSurface(image: Image, width: Int, height: Int): Image {
    val surface = Surface.makeRasterN32Premul(width, height)
    val canvas = surface.canvas
    val paint = Paint()
    canvas.drawImageRect(image, Rect(0f, 0f, width.toFloat(), height.toFloat()), paint)
    return surface.makeImageSnapshot()
}

fun rotateUsingSurface(image: Image, orientation: Int): Image {
    val rotation = when (orientation) {
        1 -> 0f
        3 -> 180f
        6 -> 90f
        8 -> 270f
        else -> 0f
    }
    log("rotate thumbnail: $rotation")
    val swapWidthHeight = orientation in listOf(6, 8)
    val width = if (swapWidthHeight) image.height else image.width
    val height = if (swapWidthHeight) image.width else image.height
    val surface = Surface.makeRasterN32Premul(width, height) ?: throw IOException("Unable to create surface!")
    surface.canvas.apply {
        val w2 = image.width.toFloat() / 2f
        val h2 = image.height.toFloat() / 2f
        translate(width.toFloat() / 2f, height.toFloat() / 2f)
        rotate(rotation)
        translate(-w2, -h2)
        drawImage(image, 0f, 0f)
    }
    return surface.makeImageSnapshot()
}

suspend fun loadFolderList(folderName: String): List<Folder> = withContext(Dispatchers.IO) {
    val folder = File(folderName)
    if (folder.isDirectory.not()) throw IllegalArgumentException("${folder.name} is not a directory!")
    folder.listFiles()?.mapNotNull { file ->
        asFolder(file)
    } ?: emptyList()
}

fun asFolder(file: File): Folder? {
    return if (file.isDirectory) {
        var childDirectories = 0
        var childFiles = 0
        file.listFiles()?.forEach {
            if (it.isDirectory) childDirectories++
            if (it.isFile && it.isSupportedFile()) childFiles++
        }
        Folder(file, childDirectories, childFiles)
    } else {
        null
    }
}

private val videoSuffixList = listOf("mp4", "mov", "avi", "mkv", "webm")
private val imageSuffixList = listOf("jpg", "jpeg", "png", "webp", "dng")
fun File.isVideoFile() = extension.lowercase() in videoSuffixList
fun File.isSupportedFile() = isSupportedFileName(this)
fun isSupportedFileName(file: File) = file.extension.lowercase() in imageSuffixList// + videoSuffixList

suspend fun loadImageListWithoutDimensions(folder: File) = withContext(folderIOContext) {
    folder.listFiles().filter { isSupportedFileName(it) }.mapNotNull { file ->
        RemoteImage(file, 0, 0)
    }
}

fun Directory.containsInt(tagType: Int): Int? {
    log("contains tag: ${tagType} ${containsTag(tagType)}")
    return if (containsTag(tagType)) {
        getInt(tagType)
    } else {
        null
    }
}

fun getMetadataDimension(imgFile: File): Dimension {
    val metadata = ImageMetadataReader.readMetadata(imgFile)
    var width = -1
    var height = -1
    log("Image: ${imgFile.name}")
    metadata.directories.forEach { directory ->
        when (directory) {
            is JpegDirectory -> {
                directory.containsInt(JpegDirectory.TAG_IMAGE_WIDTH)?.let { width = it }
                directory.containsInt(JpegDirectory.TAG_IMAGE_HEIGHT)?.let { height = it }
            }
            is WebpDirectory -> {
                directory.containsInt(WebpDirectory.TAG_IMAGE_WIDTH)?.let { width = it }
                directory.containsInt(WebpDirectory.TAG_IMAGE_HEIGHT)?.let { height = it }
            }
            is PngDirectory -> {
                directory.containsInt(PngDirectory.TAG_IMAGE_WIDTH)?.let { width = it }
                directory.containsInt(PngDirectory.TAG_IMAGE_HEIGHT)?.let { height = it }
            }
        }
    }
    if (width > 0 && height > 0) {
        log("Image detected: $width x $height")
        return Dimension(width, height)
    } else {
        throw IOException("Not a known image file: " + imgFile.absolutePath)
    }
}

class ThumbNail(val data: ByteArray, val orientation: Int, val fullWidth: Int, val fullHeight: Int)

suspend fun loadExifThumbnail(file: File): ThumbNail? = withContext(diskIOContext) {
    try {
        val metadata = JpegMetadataReader.readMetadata(file, listOf(ThumbnailReader(), JpegReader()))
        val data = metadata.getFirstDirectoryOfType(ExifThumbnailDirectory::class.java)?.let { tnDirectory ->
            tnDirectory.getObject(ThumbnailReader.TAG_THUMBNAIL_DATA) as? ByteArray
        }
        data?.let { thumb ->
            var orientation = 1
            var width = 0
            var height = 0
            metadata.getFirstDirectoryOfType(ExifIFD0Directory::class.java)?.let { directory ->
                directory.containsInt(ExifIFD0Directory.TAG_ORIENTATION)?.let { orientation = it }
                log ("ExifIFD0 orientation: $orientation")
            }
            metadata.getFirstDirectoryOfType(JpegDirectory::class.java)?.let { directory ->
                width = directory.imageWidth
                height = directory.imageHeight
                log("Image: $width x $height ")
            }
             ThumbNail(thumb, orientation, width, height)
        }
    } catch (err: Throwable) {
        log(err)
        null
    }
}

class ThumbnailReader : ExifReader() {
    companion object {
        const val TAG_THUMBNAIL_DATA = 0x10000
    }
    override fun readJpegSegments(
        segments: MutableIterable<ByteArray>,
        metadata: Metadata,
        segmentType: JpegSegmentType?
    ) {
        super.readJpegSegments(segments, metadata, segmentType)
        segments.forEach { segmentBytes ->
            // Filter any segments containing unexpected preambles
            if (startsWithJpegExifPreamble(segmentBytes)) {
                // Extract the thumbnail
                try {
                    val tnDirectory = metadata.getFirstDirectoryOfType(ExifThumbnailDirectory::class.java)
                    if (tnDirectory != null && tnDirectory.containsTag(ExifThumbnailDirectory.TAG_THUMBNAIL_OFFSET)) {
                        val offset = tnDirectory.getInt(ExifThumbnailDirectory.TAG_THUMBNAIL_OFFSET)
                        val length = tnDirectory.getInt(ExifThumbnailDirectory.TAG_THUMBNAIL_LENGTH)
                        val tnData = ByteArray(length)
                        System.arraycopy(segmentBytes, JPEG_SEGMENT_PREAMBLE.length + offset, tnData, 0, length)
                        tnDirectory.setObject(TAG_THUMBNAIL_DATA, tnData)
                    }
                } catch (e: MetadataException) {
                    log(e)
                }
            }
        }
    }
}
