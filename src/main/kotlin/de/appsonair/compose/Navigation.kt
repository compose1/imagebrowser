package de.appsonair.compose

import androidx.compose.runtime.*
import log

class NavigationStack<T: Any>(
    private val init: T,
    private val resetOnDispose: Boolean = false,
    private val defaultEnterTransition: CrossTransitionModifier? = null,
    private val defaultExitTransition: CrossTransitionModifier? = null) {

    private data class TransitionItem<T>(
        val item: SavedStateData<T>,
        val enterTransition: CrossTransitionModifier?,
        val exitTransition: CrossTransitionModifier?
    )

    private val backStackData = mutableListOf<TransitionItem<T>>()
    private lateinit var currentTransitionState: MutableState<Transition<T>>

    init {
        reset()
    }

    private fun reset() {
        log("Reset navigation stack")
        backStackData.clear()
        val item = SavedStateData(init, emptyMap())
        backStackData.add(TransitionItem(item, defaultEnterTransition, defaultExitTransition))
        currentTransitionState = mutableStateOf(Transition(item, defaultEnterTransition))
    }

    /**
     * This function should only be used once for one NavigationStack instance.
     */
    @Composable
    fun getTransition(): Transition<T> {
        DisposableEffect(Unit) {
            onDispose {
                if (resetOnDispose) {
                    reset()
                }
            }
        }
        return currentTransitionState.value
    }
    //fun current(): T = currentTransitionState.value.data.data

    fun next(next: T, enterTransition: CrossTransitionModifier? = defaultEnterTransition, exitTransition: CrossTransitionModifier? = defaultExitTransition) {
        val nextItem = SavedStateData(next, emptyMap())
        backStackData.add(TransitionItem(nextItem, enterTransition, exitTransition))
        currentTransitionState.value = Transition(nextItem, enterTransition)
    }
    fun back() = back(null, false)

    /**
     * Override the exit transition which was set in next(..)
     */
    fun back(exitTransition: CrossTransitionModifier?) = back(exitTransition, true)

    private fun back(exitTransition: CrossTransitionModifier?, overrideTransition: Boolean) =
        if (backStackData.size <= 1) {
            false
        } else {
            val transition = if (overrideTransition) exitTransition else backStackData.last().exitTransition
            backStackData.removeAt(backStackData.lastIndex)
            val pData = backStackData.last().item
            currentTransitionState.value = Transition(pData, transition)
            true
        }
}
