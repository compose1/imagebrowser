package de.appsonair.compose

import androidx.compose.animation.core.*
import androidx.compose.foundation.layout.Box
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveableStateHolder
import androidx.compose.ui.*
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.layout.LayoutModifier
import androidx.compose.ui.layout.Measurable
import androidx.compose.ui.layout.MeasureResult
import androidx.compose.ui.layout.MeasureScope
import androidx.compose.ui.unit.Constraints
import log
import kotlin.math.min
import kotlin.math.roundToInt

data class SavedStateData<T>(val data: T, var savedState: Map<String, List<Any?>>?)

data class Transition<T>(val data: SavedStateData<T>, val effect: CrossTransitionModifier?)
data class SimpleTransition<T>(val data: T, val effect: CrossTransitionModifier?, val anim: AnimationSpec<Float> = tween())

data class RelativeOffsetPxModifier(val x: Float = 0f, val y: Float = 0f) : LayoutModifier {
    override fun MeasureScope.measure(
        measurable: Measurable,
        constraints: Constraints
    ): MeasureResult {
        val placeable = measurable.measure(constraints)
        return layout(placeable.width, placeable.height) {
            placeable.place((x * placeable.width).roundToInt(), (y * placeable.height).roundToInt())
        }
    }
}

val tweenAnimation = tween<Float>(500)

enum class ItemTransitionState {
    BecomingVisible, BecomingNotVisible;
}

val slideRollRightTransition: CrossTransitionModifier = { progress, state ->
    val p = when(state) {
        ItemTransitionState.BecomingVisible -> -(1f - progress)
        else -> progress
    }
    RelativeOffsetPxModifier(x = p)
}
val slideRollLeftTransition: CrossTransitionModifier = { progress, state ->
    val p = when(state) {
        ItemTransitionState.BecomingVisible -> 1f - progress
        else -> -progress
    }
    RelativeOffsetPxModifier(x = p)
}
val slideInTransition: CrossTransitionModifier = { progress, state ->
    val target = state == ItemTransitionState.BecomingVisible
    val p = if (target) 1f-progress else 0f
    val zIndex = if (target) 1f else 0f
    RelativeOffsetPxModifier(y = p).zIndex(zIndex)
}
val slideOutTransition: CrossTransitionModifier = { progress, state ->
    val target = state == ItemTransitionState.BecomingVisible
    val p = if (target) 0f else progress
    val zIndex = if (target) 0f else 1f
    RelativeOffsetPxModifier(y = p).zIndex(zIndex)
}

val crossfadeTransition: CrossTransitionModifier = { progress, state ->
    val p = if (state == ItemTransitionState.BecomingVisible) progress else 1f - progress
    Modifier.alpha(p)
}
val fadeInTransition: CrossTransitionModifier = { progress, state ->
    val target = state == ItemTransitionState.BecomingVisible
    val p = if (target) min(progress, .99f) else 1f
    val zIndex = if (target) 1f else 0f
    Modifier.zIndex(zIndex).alpha(p)
}
val fadeOutTransition: CrossTransitionModifier = { progress, state ->
    val target = state == ItemTransitionState.BecomingVisible
    val p = if (target) 1f else min(1f - progress, .99f)
    val zIndex = if (target) 0f else 1f
    Modifier.zIndex(zIndex).alpha(p)
}

val fadeOverTransition: CrossTransitionModifier = { progress, state ->
    val target = state == ItemTransitionState.BecomingVisible
    val p = if (target) min(.5f, progress) * 2f else min(.5f,1f-progress) * 2f
    val zIndex = if (target) 1f else 0f
    Modifier.zIndex(zIndex).alpha(p)
}

/**
 * This function returns the Modifier which is applied to both screens the current and the new one.
 * progress -> is the progress of the transition 0f..1f
 * state -> is true when the modifier is applied to the new screen
 *                 and false for the screen which is replaced by the new screen
 */
typealias CrossTransitionModifier = (progress: Float, state: ItemTransitionState) -> Modifier


@Composable
fun <T: Any>NavigationSwitcher(transition: Transition<T>,
                               children: @Composable() (T) -> Unit) {
    val item = transition.data
    val saveableStateHolder = rememberSaveableStateHolder()
    saveableStateHolder.SaveableStateProvider(item.data) {
        children(item.data)
    }
}



private data class TransitionAnimationItem<T>(
    val key: T,
    val content: @Composable () -> Unit
)

@Composable
fun <T> TransitionSwitcher(
    targetState: SimpleTransition<T>,
    modifier: Modifier = Modifier,
    animationSpec: FiniteAnimationSpec<Float> = tween(1000),
    content: @Composable (T) -> Unit
) {
    val items = remember { mutableStateListOf<TransitionAnimationItem<SimpleTransition<T>>>() }
    val transitionState = remember { MutableTransitionState(targetState) }
    val targetChanged = (targetState != transitionState.targetState)
    transitionState.targetState = targetState
    val transition = updateTransition(transitionState)
    if (targetChanged || items.isEmpty()) {
        // Only manipulate the list when the state is changed, or in the first run.
        val keys = items.map { it.key }.run {
            if (!contains(targetState)) {
                toMutableList().also { it.add(targetState) }
            } else {
                this
            }
        }
        items.clear()
        keys.mapTo(items) { key ->
            TransitionAnimationItem(key) {
                val progress by transition.animateFloat(
                    transitionSpec = { animationSpec },
                    targetValueByState = { state -> if (state == key) 1f else 0f }
                )
                //TODO use transition modifier
                val direction = if (targetState == key) ItemTransitionState.BecomingVisible else ItemTransitionState.BecomingNotVisible
                //log("Target: ${targetState.data} key:${key.data} transitionState: ${direction.name}")
                val transitionProgress = if (direction == ItemTransitionState.BecomingVisible) progress else 1f - progress
                val effectModifier = key.effect?.invoke(transitionProgress, direction) ?: Modifier.alpha(progress)
                Box(effectModifier) {
                    content(key.data)
                }
            }
        }
    } else if (transitionState.currentState == transitionState.targetState) {
        // Remove all the intermediate items from the list once the animation is finished.
        items.removeAll { it.key != transitionState.targetState }
    }

    Box(modifier) {
        if (items.size > 1) {
            items.forEach {
                key(it.key) {
                    it.content()
                }
            }
        } else {
            content(targetState.data)
        }
    }
}
