package de.appsonair.compose

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.items
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

/**
 * apps on air
 *
 * @author Timo Drick
 */
@Composable
fun <T> MultiColumnFor(columns: Int, items: List<T>, modifier: Modifier = Modifier, padding: Dp = 0.dp, state: LazyListState, itemCallback: @Composable (T) -> Unit) {
    LazyColumn(contentPadding = PaddingValues(bottom = padding), state = state) {
        items(items.chunked(columns)) { rowList ->
            Row(Modifier.padding(top = padding, start = padding)) {
                val rowModifier = Modifier.weight(1f).padding(end = padding).align(Alignment.CenterVertically)
                rowList.forEach {
                    Box(rowModifier) {
                        itemCallback(it)
                    }
                }
                val emptyRows = (columns - rowList.size)
                repeat(emptyRows) {
                    Spacer(modifier = rowModifier)
                }
            }
        }
    }
}
