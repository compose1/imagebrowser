import androidx.compose.material.MaterialTheme
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.input.key.Key
import androidx.compose.ui.input.key.KeyEventType
import androidx.compose.ui.input.key.key
import androidx.compose.ui.input.key.type
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.*
import imagebrowser.DesktopApp
import kotlinx.serialization.Serializable
import java.awt.Toolkit
import java.io.File

@Serializable
data class WindowSettings(
    val positionX: Float,
    val positionY: Float,
    val width: Float,
    val height: Float,
    val fullscreen: Boolean
)

@OptIn(ExperimentalComposeUiApi::class)
fun main() {
    Toolkit.getDefaultToolkit().apply {
        log("Screen size: $screenSize resolution: $screenResolution") // maybe use this values to calculate initial window size
    }
    val settingsFile = PersistentStorage(WindowSettings.serializer(), File(".imagebrowser.settings"),
        default = WindowSettings(0f, 0f, 500f, 500f, false)
    )
    val windowSettings = settingsFile.load()
    val windowState = WindowState(
        placement = if (windowSettings.fullscreen) WindowPlacement.Fullscreen else WindowPlacement.Floating,
        position = WindowPosition.Absolute(x = windowSettings.positionX.dp, y = windowSettings.positionY.dp),
        size = DpSize(width = windowSettings.width.dp, height = windowSettings.height.dp)
    )
    application {
        Window(
            title = "Image Browser",
            icon = painterResource("mango_icon.png"),
            state = windowState,
            onCloseRequest = {
                val newSettings = WindowSettings(
                    positionX = windowState.position.x.value,
                    positionY = windowState.position.y.value,
                    width = windowState.size.width.value,
                    height = windowState.size.height.value,
                    fullscreen = windowState.placement == WindowPlacement.Fullscreen
                )
                settingsFile.save(newSettings)
                exitApplication()
            },
            onKeyEvent = { event ->
                log("Key event: $event")
                when {
                    event.key == Key.F && event.type == KeyEventType.KeyDown -> {
                        windowState.placement = if (windowState.placement == WindowPlacement.Fullscreen)
                            WindowPlacement.Floating
                        else
                            WindowPlacement.Fullscreen
                        true
                    }
                    else -> false
                }
            }
        ) {
            MaterialTheme {
                DesktopApp()
            }
        }
    }
}
