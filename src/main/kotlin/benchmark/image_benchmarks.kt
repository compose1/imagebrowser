package benchmark

import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.asComposeImageBitmap
import de.appsonair.compose.loadExifThumbnail
import de.appsonair.compose.scaleUsingSurface
import kotlinx.coroutines.runBlocking
import log
import org.jetbrains.skia.*
import java.awt.image.BufferedImage
import java.awt.image.DataBufferInt
import java.io.ByteArrayOutputStream
import java.io.File
import javax.imageio.ImageIO
import kotlin.math.roundToInt

/**
 * apps on air
 *
 * @author Timo Drick
 */

val imageDirectoryPng = "/home/timo/Pictures/demo/CLIC_sources_2048x1320"
val imageDirectoryJpeg = "/home/timo/Pictures/demo/star wars wallpaper"
val imageDirectoryBigData = "/media/timo/media2/2015_Philippinen/gh4/photos/"
val imageDirectoryAmrum = "/home/timo/mnt/media/2016/Amrum/jpg"

fun main() {
    val file = File("/media/timo/media/2019/Philippinen/Barbara/DSC_0060.JPG")
    runBlocking {
        val thumbNail = loadExifThumbnail(file)

    }
    benchmark(imageDirectoryPng)
    /*File(imageDirectoryAmrum).listFiles().take(10).forEach { file ->
        log("File: ${file.name}")
        val bytes = loadExifThumbnail(file)
        log("Loaded thumbnaildata size: ${bytes?.size}")
        try {
            if (bytes != null) {
                val image = Image.makeFromEncoded(bytes)
                log("Image loaded")
            } else {
                log("No thumbnail data detected!")
            }
        } catch (err: Throwable) {
            log("Np thumbnail data detected!")
        }
    }*/
}

fun benchmark(directory: String) {
    val folder = File(directory)
    if (folder.isDirectory.not()) throw IllegalArgumentException("${folder.name} is not a directory!")
    val fileList = folder.listFiles()
    val byteArrayData = BenchTiming.measure("file loading") {
        fileList.map { it.readBytes() }
    }

    // read to skia Image
    val imageList = BenchTiming.measure("Image.makeFromEncoded()") {
        byteArrayData.map { Image.makeFromEncoded(it) }
    }

    // scale
    val scaledImageList: List<Image> = BenchTiming.measure("scale image using surface") {
        imageList.map { fullImage ->
            val width = 512
            val ratio = fullImage.width.toFloat() / fullImage.height.toFloat()
            val height = (width.toFloat() / ratio).roundToInt()
            scaleUsingSurface(fullImage, fullImage.width / 4, fullImage.height / 4)
        }
    }
    // scale
    val scaledImageIRectList = BenchTiming.measure("scale image using surface and IRect") {
        imageList.map { fullImage ->
            val width = 512
            val ratio = fullImage.width.toFloat() / fullImage.height.toFloat()
            val height = (width.toFloat() / ratio).roundToInt()
            scaleUsingRectBitmap(fullImage, fullImage.width / 4, fullImage.height / 4)
        }
    }

    //compress to jpeg
    BenchTiming.measure("compress to using Image.encodeToData(JPEG, 100)") {
        scaledImageList.forEach {
            compressUsingImage(it, EncodedImageFormat.JPEG, 80)
        }
    }
    //compress to png
    /*BenchTiming.measure("compress to using Image.encodeToData(PNG, 100)") {
        scaledImageList.forEach {
            compressUsingImage(it, EncodedImageFormat.PNG, 80)
        }
    }*/
    //compress to webp
    BenchTiming.measure("compress to using Image.encodeToData(WEBP, 100)") {
        scaledImageList.forEach {
            compressUsingImage(it, EncodedImageFormat.WEBP, 80)
        }
    }
}

fun scaleUsingBitmap(image: Image, width: Int, height: Int): ImageBitmap {
    val bitmap = Bitmap()
    bitmap.allocPixels(ImageInfo.makeN32(width, height, ColorAlphaType.PREMUL))
    val canvas = Canvas(bitmap)
    val paint = Paint().apply {
        //TODO
        //filterQuality = FilterQuality.HIGH
    }
    canvas.drawImageRect(image, Rect(0f, 0f, width.toFloat(), height.toFloat()), paint)
    bitmap.setImmutable()
    return bitmap.asComposeImageBitmap()
}

fun compressUsingImageIO(asset: ImageBitmap, format: String): ByteArray {
    // compress data to jpeg for caching
    val scaledImage = BufferedImage(asset.width, asset.height, BufferedImage.TYPE_INT_ARGB)
    val intArray = (scaledImage.raster.dataBuffer as DataBufferInt).data
    asset.readPixels(intArray)
    val baos = ByteArrayOutputStream()
    ImageIO.write(scaledImage, format, baos)
    return baos.toByteArray()
}

fun compressUsingImage(image: Image, format: EncodedImageFormat, quality: Int) {
    image.encodeToData(format, quality)!!.bytes
}

fun scaleUsingRectBitmap(image: Image, width: Int, height: Int) {
    val bitmap = Bitmap()
    bitmap.allocN32Pixels(width, height, true)
    val canvas = Canvas(bitmap)
    //val surface = Surface.makeRasterN32Premul(image.width, image.height)
    val paint = Paint().apply {
        //TODO
        //filterQuality = FilterQuality.HIGH
    }
    canvas.drawImageRect(image, Rect(0f, 0f, width.toFloat(), height.toFloat()), paint)
    bitmap.setImmutable()
}

class BenchTiming(private val name: String) {
    companion object {
        fun <T>measure(name: String, block: () -> T): T {
            val t = BenchTiming(name)
            t.start()
            val result = block()
            t.stop()
            return result
        }
    }
    private var ts: Long = 0
    fun start() {
        ts = System.currentTimeMillis()
    }

    fun stop() {
        val delta = System.currentTimeMillis() - ts
        log("Timing: $name delta: $delta ms")
    }
}