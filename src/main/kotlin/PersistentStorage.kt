import kotlinx.serialization.KSerializer
import kotlinx.serialization.json.Json
import java.io.File

/**
 * apps on air
 *
 * @author Timo Drick
 */

class PersistentStorage<T>(
    private val serializer: KSerializer<T>,
    val file: File,
    private val default: T
) {
    val json = Json

    fun save(value: T) {
        val jsonStr = json.encodeToString(serializer, value)
        file.writeText(jsonStr)
    }
    fun load(): T {
        return try {
            val jsonStr = file.readText()
            json.decodeFromString(serializer, jsonStr) ?: default
        } catch (err: Throwable) {
            log(err)
            default
        }
    }
}

