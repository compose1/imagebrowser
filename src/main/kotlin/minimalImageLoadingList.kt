import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.toComposeImageBitmap
import androidx.compose.ui.window.singleWindowApplication
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.jetbrains.skia.Image
import java.io.IOException
import java.net.URL

data class RemoteImage(val url: String, val width: Int, val height: Int)

@Throws(IOException::class)
suspend fun loadImageUrl(url: String) = withContext(Dispatchers.IO) {
    val bytes = URL(url).openStream().use { it.readAllBytes() }
    log("Decode bytes: ${bytes.size}")
    Image.makeFromEncoded(bytes).toComposeImageBitmap()
}

@Composable
fun loadImageAsyn(url: String): ImageBitmap? {
    var state: ImageBitmap? by remember(url) { mutableStateOf(null) }
    LaunchedEffect(url) {
        state = try {
            loadImageUrl(url)
        } catch (err: Exception) {
            println("Unable to load image $url: ${err.message}")
            null
        }
    }
    return state
}


fun main() {
    val width = 1000
    val height = 600
    val remoteImageList = (0..500).map {
        RemoteImage(
            url = "https://picsum.photos/id/$it/$width/$height",
            width = width,
            height = height
        )
    }
    singleWindowApplication(
        title = "Image Browser",
        //size = IntSize(200, 800),
        //centered = true,
    ) {
        LazyColumn {
            items(remoteImageList) { item ->
                val image = loadImageAsyn(item.url)
                val aspectRatio = item.width.toFloat() / item.height.toFloat()
                Box(Modifier.aspectRatio(aspectRatio).fillMaxWidth()) {
                    image?.let {
                        Image(modifier = Modifier.fillMaxSize(), bitmap = it, contentDescription = "image")
                    }
                }
            }
        }
    }
}
