package imagebrowser

import androidx.compose.animation.core.*
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.focusable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.ArrowForward
import androidx.compose.material.icons.filled.KeyboardArrowUp
import androidx.compose.runtime.Composable
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.toComposeImageBitmap
import androidx.compose.ui.input.key.*
import androidx.compose.ui.input.pointer.pointerMoveFilter
import androidx.compose.ui.unit.dp
import de.appsonair.compose.*
import de.appsonair.compose.TransitionSwitcher
import kotlinx.coroutines.*
import log
import java.io.File
import kotlin.math.max
import kotlin.math.min

/**
 * apps on air
 *
 * @author Timo Drick
 */
sealed class CurrentImage {
    object Start: CurrentImage()
    data class Img(val image: ImageBitmap, val index: Int): CurrentImage() {
        override fun toString() = "img:$index"
    }
    data class Video(val file: File, val index: Int): CurrentImage()
    data class Error(val errorMessage: String, val err: Throwable): CurrentImage()
}

data class NextImage(val position: Int, val forward: Boolean)
data class PreloadedImage(val position: Int, val image: ImageBitmap)

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun DetailScreen(imageSelection: ImageListSelection, onFinished: () -> Unit) {
    var position by rememberState { NextImage(imageSelection.selected, true) }
    var currentImage by rememberState { SimpleTransition<CurrentImage>(CurrentImage.Start, fadeInTransition) }
    var isLoadingSpinnerVisible by rememberState { false }
    var preloadedImage: PreloadedImage? by rememberState { null }
    var preloadJob: Job? = null
    LaunchedEffect(position) {
        System.gc()
        val preloaded = preloadedImage
        val newImg = if (preloaded?.position == position.position) {
            CurrentImage.Img(preloaded.image, position.position)
        } else {
            val imageDetails = imageSelection.imageList[position.position]
            val loadingSpinner = async {
                delay(300)
                isLoadingSpinnerVisible = true
            }
            try {
                val asset = loadImageFile(imageDetails.file).toComposeImageBitmap()
                CurrentImage.Img(asset, position.position)
            } catch (err: Throwable) {
                CurrentImage.Error(err.localizedMessage, err)
            } finally {
                loadingSpinner.cancelAndJoin()
                isLoadingSpinnerVisible = false
            }
        }
        val duration = 800
        val anim = tween<Float>(duration, easing = LinearOutSlowInEasing)
        val transition = when {
            currentImage.data is CurrentImage.Start -> fadeInTransition
            position.forward -> fadeOverTransition
            else -> fadeOverTransition
        }
        //preload next image
        preloadJob?.cancel() // Cancel currently running preload jobs
        preloadJob = async {
            delay(duration.toLong()) // wait until animation is finished
            val nextPos = position.position + 1
            if (imageSelection.imageList.size > nextPos) {
                val imageDetails = imageSelection.imageList[nextPos]
                try {
                    log("Preload image: $nextPos ...")
                    val image = loadImageFile(imageDetails.file).toComposeImageBitmap()
                    preloadedImage = PreloadedImage(nextPos, image)
                } catch (err: Throwable) {
                    // Ignore error
                }
            }
            System.gc()
        }
        currentImage = SimpleTransition(newImg, transition, anim)
    }
    fun previousImage() {
        position = NextImage(max(0, position.position - 1), false)
    }
    fun nextImage() {
        position = NextImage(min(imageSelection.imageList.size - 1, position.position + 1), true)
    }
    val navbarOpacity = remember { Animatable(1f) }
    LaunchedEffect(imageSelection) {
        delay(1000)
        navbarOpacity.animateTo(0f, tween(2000))
    }
    val scope = rememberCoroutineScope()
    val hoverFilter = Modifier.pointerMoveFilter(
        onEnter = {
            false
                  },
        onMove = {
            scope.launch {
                navbarOpacity.animateTo(1f, tween(500))
                navbarOpacity.animateTo(0f, tween(1000, 1000))
            }
            false
        }
    )
    val focusRequester = remember { FocusRequester() }
    LaunchedEffect(Unit) {
        focusRequester.requestFocus()
    }
    Box(Modifier
        .fillMaxSize()
        .focusable()
        .focusRequester(focusRequester)
        .onPreviewKeyEvent { event ->
            var consumed = true
            if (event.type == KeyEventType.KeyDown) {
                when(event.key) {
                    Key.Spacebar,
                    Key.DirectionRight,
                    Key.SystemNavigationRight -> nextImage()
                    Key.DirectionLeft,
                    Key.Back,
                    Key.SystemNavigationLeft -> previousImage()
                    Key.Escape -> onFinished()
                    else -> consumed = false
                }
            } else {
                consumed = false
            }
            consumed
        }
    ) {
        TransitionSwitcher(currentImage) {
            when(val state = it) {
                is CurrentImage.Img -> {
                    Image(
                        modifier = Modifier.fillMaxSize(),
                        bitmap = state.image,
                        contentDescription = ""
                    )
                }
                is CurrentImage.Error -> ErrorBox(message = state.errorMessage)
                CurrentImage.Start -> {}
                is CurrentImage.Video -> {}
            }
        }
        Column(Modifier.fillMaxSize().alpha(navbarOpacity.value)) {
            val navBackgroundColor = MaterialTheme.colors.background.copy(alpha = 0.8f)
            Row(hoverFilter.fillMaxWidth().background(navBackgroundColor)) {
                IconButton(onClick = { onFinished() }) {
                    Icon(imageVector = Icons.Default.KeyboardArrowUp, contentDescription = "up")
                }
            }
            Box(Modifier.fillMaxWidth().weight(1f)) {
                Box(
                    hoverFilter.fillMaxHeight().fillMaxWidth(.1f).align(Alignment.CenterStart)
                        .background(navBackgroundColor).clickable { previousImage() }
                ) {
                    Icon(modifier = Modifier.align(Alignment.Center), imageVector = Icons.Default.ArrowBack, contentDescription = "back")

                }
                Box(
                    hoverFilter.fillMaxHeight().fillMaxWidth(.1f).align(Alignment.CenterEnd)
                        .background(navBackgroundColor).clickable { nextImage() }
                ) {
                    Icon(modifier = Modifier.align(Alignment.Center), imageVector = Icons.Default.ArrowForward, contentDescription = "forward")
                }
                if (isLoadingSpinnerVisible) {
                    LoadingBox(
                        Modifier
                            .align(Alignment.Center)
                            .background(Color.LightGray.copy(alpha = 0.8f), shape = RoundedCornerShape(6.dp))
                            .padding(12.dp)
                    )
                }
            }
        }
    }
}
