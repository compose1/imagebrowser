package imagebrowser

import androidx.compose.animation.*
import androidx.compose.animation.core.*
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.ArrowForward
import androidx.compose.material.icons.filled.KeyboardArrowUp
import androidx.compose.runtime.Composable
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toComposeImageBitmap
import androidx.compose.ui.input.key.*
import androidx.compose.ui.input.pointer.PointerEventType
import androidx.compose.ui.input.pointer.onPointerEvent
import androidx.compose.ui.unit.dp
import de.appsonair.compose.*
import kotlinx.coroutines.*
import log
import kotlin.math.max
import kotlin.math.min

/**
 * apps on air
 *
 * @author Timo Drick
 */
@OptIn(ExperimentalComposeUiApi::class, ExperimentalAnimationApi::class)
@Composable
fun DetailScreen2(imageSelection: ImageListSelection, onFinished: () -> Unit) {
    val duration = 800
    var position by rememberState { NextImage(imageSelection.selected, true) }
    var currentImage: CurrentImage by rememberState { CurrentImage.Start }
    var isLoadingSpinnerVisible by rememberState { false }
    var preloadedImage: PreloadedImage? by rememberState { null }
    var preloadJob: Job? = null
    LaunchedEffect(position) {
        preloadJob?.cancel() // Cancel currently running preload jobs
        System.gc()
        val imageDetails = imageSelection.imageList[position.position]
        val loadingSpinner = async {
            delay(300)
            isLoadingSpinnerVisible = true
        }
        currentImage = try {
            if (imageDetails.file.isVideoFile()) {
                CurrentImage.Video(imageDetails.file, position.position)
            } else {
                val preloaded = preloadedImage
                val asset = if (preloaded?.position == position.position) preloaded.image
                else
                    loadImageFile(imageDetails.file).toComposeImageBitmap()
                CurrentImage.Img(asset, position.position)
            }
        } catch (err: Throwable) {
            CurrentImage.Error(err.localizedMessage, err)
        } finally {
            loadingSpinner.cancelAndJoin()
            isLoadingSpinnerVisible = false
        }
        //preload next image
        preloadJob = async {
            delay(duration.toLong()) // wait until animation is finished
            val nextPos = position.position + 1
            if (imageSelection.imageList.size > nextPos) {
                val img = imageSelection.imageList[nextPos]
                if (img.file.isVideoFile().not()) {
                    try {
                        log("Preload image: $nextPos ...")
                        val image = loadImageFile(img.file).toComposeImageBitmap()
                        preloadedImage = PreloadedImage(nextPos, image)
                    } catch (err: Throwable) {
                        // Ignore error
                    }
                }
            }
            System.gc()
        }
   }
    fun previousImage() {
        position = NextImage(max(0, position.position - 1), false)
    }
    fun nextImage() {
        position = NextImage(min(imageSelection.imageList.size - 1, position.position + 1), true)
    }
    val navbarOpacity = remember { Animatable(1f) }
    LaunchedEffect(imageSelection) {
        delay(1000)
        navbarOpacity.animateTo(0f, tween(2000))
    }
    val scope = rememberCoroutineScope()
    val hoverFilter = Modifier.onPointerEvent(PointerEventType.Move) {
        scope.launch {
            navbarOpacity.animateTo(1f, tween(500))
            navbarOpacity.animateTo(0f, tween(1000, 1000))
        }
    }
    val focusRequester = remember { FocusRequester() }
    LaunchedEffect(Unit) {
        focusRequester.requestFocus()
    }
    Box(Modifier
        .fillMaxSize()
        .focusable()
        .focusRequester(focusRequester)
        .onPreviewKeyEvent { event ->
            var consumed = true
            if (event.type == KeyEventType.KeyDown) {
                when(event.key) {
                    Key.Spacebar,
                    Key.DirectionRight,
                    Key.SystemNavigationRight -> nextImage()
                    Key.DirectionLeft,
                    Key.Back,
                    Key.SystemNavigationLeft -> previousImage()
                    Key.Escape -> onFinished()
                    else -> consumed = false
                }
            } else {
                consumed = false
            }
            consumed
        }
    ) {
        AnimatedContent(
            modifier = Modifier.align(Alignment.Center),
            targetState = currentImage,
            transitionSpec = {
                EnterTransition.None with ExitTransition.None
            }
        ) {
            when(val state = it) {
                is CurrentImage.Img -> {
                    Image(
                        modifier = Modifier.fillMaxSize(),
                        bitmap = state.image,
                        contentDescription = ""
                    )
                }
                is CurrentImage.Video -> {
                    val videoplayerState = remember(state.file) { VideoPlayerState() }
                    VideoPlayerDirect(
                        modifier = Modifier.fillMaxHeight().aspectRatio(videoplayerState.aspectRatio),
                        state = videoplayerState,
                        url = state.file.toString()
                    )
                }
                is CurrentImage.Error -> ErrorBox(message = state.errorMessage)
                CurrentImage.Start -> {}
            }
        }
        Column(Modifier.fillMaxSize().alpha(navbarOpacity.value)) {
            val navBackgroundColor = MaterialTheme.colors.background.copy(alpha = 0.8f)
            Row(hoverFilter.fillMaxWidth().background(navBackgroundColor)) {
                IconButton(onClick = { onFinished() }) {
                    Icon(imageVector = Icons.Default.KeyboardArrowUp, contentDescription = "up")
                }
            }
            Box(Modifier.fillMaxWidth().weight(1f)) {
                Box(
                    hoverFilter.fillMaxHeight().fillMaxWidth(.1f).align(Alignment.CenterStart)
                        .background(navBackgroundColor).clickable { previousImage() }
                ) {
                    Icon(modifier = Modifier.align(Alignment.Center), imageVector = Icons.Default.ArrowBack, contentDescription = "back")

                }
                Box(
                    hoverFilter.fillMaxHeight().fillMaxWidth(.1f).align(Alignment.CenterEnd)
                        .background(navBackgroundColor).clickable { nextImage() }
                ) {
                    Icon(modifier = Modifier.align(Alignment.Center), imageVector = Icons.Default.ArrowForward, contentDescription = "forward")
                }
                if (isLoadingSpinnerVisible) {
                    LoadingBox(
                        Modifier
                            .align(Alignment.Center)
                            .background(Color.LightGray.copy(alpha = 0.8f), shape = RoundedCornerShape(6.dp))
                            .padding(12.dp)
                    )
                }
            }
        }
    }
}
