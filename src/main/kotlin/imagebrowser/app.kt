package imagebrowser

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import de.appsonair.compose.*
import log
import java.io.File
import kotlin.math.roundToInt

/**
 * apps on air
 *
 * @author Timo Drick
 */

private sealed class MainUIScreen {
    object Start: MainUIScreen()
    data class Detail(val selected: ImageListSelection): MainUIScreen()
}

data class ImageListSelection(val imageList: List<RemoteImage>, val selected: Int)

val lightThemeColors = Colors(
    primary = Color(0xFFFAB24A),
    primaryVariant = Color(0xFF7D4433),
    onPrimary = Color.White,
    secondary = Color(0xFF448AFF),
    secondaryVariant = Color(0xFF376ECC),
    onSecondary = Color.White,
    background = Color(0xFFFAB24A),
    onBackground = Color.Black,
    surface = Color(0xFFFFD69C),
    onSurface = Color.Black,
    error = Color(0xFFD00036),
    onError = Color.White,
    isLight = true
)

private val navigation = NavigationStack<MainUIScreen>(MainUIScreen.Start, defaultEnterTransition = fadeInTransition, defaultExitTransition = fadeOutTransition)

@Composable
fun DesktopApp() {
    MaterialTheme(colors = lightThemeColors) {
        MainContent()
    }
}

@Composable
fun MainContent() {
    val currentFolder = rememberSaveable {
        mutableStateOf(System.getProperty("user.home"))
    }
    Box(Modifier.wrapContentSize().fillMaxSize().background(MaterialTheme.colors.background)) {
        NavigationSwitcher(transition = navigation.getTransition()) { screen ->
            when (screen) {
                is MainUIScreen.Start -> {
                    FolderScreen(
                        currentFolder = currentFolder.value,
                        onFolderChanged = { currentFolder.value = it },
                        onImageSelected = { navigation.next(MainUIScreen.Detail(it)) }
                    )
                }
                is MainUIScreen.Detail -> {
                    DetailScreen2(screen.selected, onFinished = { navigation.back() })
                }
            }
        }
    }
}

@Composable
fun <T> rememberState(initBlock: () -> T): MutableState<T> = remember { mutableStateOf(initBlock()) }

@Composable
fun StartScreen(folder: MutableState<String>, onImageSelected: (ImageListSelection) -> Unit) {
    var currentFolder by folder
    log("Folder: ${currentFolder}")
    Row(Modifier.fillMaxSize()) {
        Box(Modifier.width(200.dp)) {
            val state = loadingStateFor(currentFolder) { loadFolderList(currentFolder) }
            when (state) {
                is LoadingState.Loading -> LoadingBox()
                is LoadingState.Success -> {
                    val folderList = state.data.filter { it.file.isHidden.not() }.sortedBy { it.file.name }
                    log("Folder list: ${folderList.size}")
                    Column {
                        Box(
                            Modifier.clickable {
                                val parent = File(currentFolder).parentFile
                                if (parent != null && parent.exists() && parent.isDirectory) {
                                    currentFolder = File(currentFolder).parentFile.absolutePath
                                }
                            }.fillMaxWidth().padding(4.dp)
                                .background(MaterialTheme.colors.primarySurface, RoundedCornerShape(4.dp))
                        ) {
                            Text(".. (to parent)", Modifier.padding(4.dp))
                        }
                        LazyColumn(modifier = Modifier.weight(1f)) {
                            items(folderList) { folder ->
                                Box(
                                    Modifier.clickable { currentFolder = folder.file.absolutePath }.fillMaxWidth()
                                        .padding(4.dp)
                                        .background(MaterialTheme.colors.primaryVariant, RoundedCornerShape(4.dp))
                                ) {
                                    Text(folder.file.name, Modifier.padding(4.dp))
                                }
                            }
                        }
                    }
                }
                is LoadingState.Error -> {
                    ErrorBox(message = state.error.message ?: "Unknown error!")
                }
                LoadingState.Start -> {}
            }
        }
        Box(Modifier.weight(1f)) {
            val imageListState = loadingStateFor(currentFolder) {
                loadImageListWithoutDimensions(File(currentFolder))
            }
            when (imageListState) {
                is LoadingState.Loading -> LoadingBox()
                is LoadingState.Success -> {
                    ImageList(modifier = Modifier.fillMaxSize(), imageList = imageListState.data, onImageClicked = {
                        val selection = ImageListSelection(imageListState.data, imageListState.data.indexOf(it))
                        onImageSelected(selection)
                    })
                }

                is LoadingState.Error -> {}
                LoadingState.Start -> {}
            }
        }
    }
}

data class RemoteImage(val file: File, val width: Int, val height: Int)


private const val GRID_ITEM_WIDTH = 200

@Composable
fun ImageList(modifier: Modifier, imageList: List<RemoteImage>, onImageClicked: (RemoteImage) -> Unit) {
    val listState = rememberLazyListState()
    val scrollbarStyle = ScrollbarStyle(
        minimalHeight = 16.dp,
        thickness = 12.dp,
        shape = RoundedCornerShape(4.dp),
        hoverDurationMillis = 1000,
        unhoverColor = Color.Black.copy(alpha = 0.3f),
        hoverColor = Color.Black.copy(alpha = 0.8f)
    )
    BoxWithConstraints(modifier = modifier) {
        val gridColumns = (constraints.maxWidth.toFloat() / GRID_ITEM_WIDTH.toFloat()).roundToInt()
        val aspectRatio = 4f / 3f
        val gridPadding = 4.dp
        MultiColumnFor(gridColumns, imageList, padding = gridPadding, state = listState) { image ->
            Box(Modifier.fillMaxWidth().aspectRatio(aspectRatio).clickable { onImageClicked(image) }) {
                ThumbnailImage(
                    modifier.fillMaxSize(),
                    image = image, overrideSize = GRID_ITEM_WIDTH
                )
                Box(Modifier.align(Alignment.TopStart).fillMaxWidth().background(MaterialTheme.colors.primaryVariant.copy(alpha = 0.8f))) {
                    Text(image.file.name, Modifier.align(Alignment.Center))
                }
            }
        }
        VerticalScrollbar(
            adapter = rememberScrollbarAdapter(listState),
            modifier = Modifier.align(Alignment.CenterEnd).fillMaxHeight(),
            style = scrollbarStyle
        )
    }
}

@Composable
fun LoadingBox(modifier: Modifier = Modifier.fillMaxSize()) {
    Box(modifier = modifier) {
        CircularProgressIndicator(Modifier.align(Alignment.Center), color = MaterialTheme.colors.secondaryVariant)
    }
}

@Composable
fun ErrorBox(modifier: Modifier = Modifier.fillMaxSize(), message: String, onRetry: (() -> Unit)? = null) {
    Box(modifier = modifier) {
        Column(modifier = Modifier.align(Alignment.Center).padding(12.dp)) {
            Text(text = message)
            if (onRetry != null) {
                Spacer(modifier = Modifier.height(6.dp))
                TextButton(modifier = Modifier.align(Alignment.CenterHorizontally), onClick = { onRetry() }) {
                    Text("Retry", color = MaterialTheme.colors.secondary)
                }
            }
        }
    }
}
