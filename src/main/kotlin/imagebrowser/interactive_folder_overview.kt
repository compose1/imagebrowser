package imagebrowser

import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.tween
import androidx.compose.foundation.*
import androidx.compose.foundation.gestures.scrollBy
import androidx.compose.foundation.gestures.scrollable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Folder
import androidx.compose.runtime.*
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.input.pointer.PointerEventType
import androidx.compose.ui.input.pointer.onPointerEvent
import androidx.compose.ui.layout.onPlaced
import androidx.compose.ui.layout.positionInRoot
import androidx.compose.ui.unit.dp
import androidx.compose.ui.zIndex
import de.appsonair.compose.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import log
import java.io.File
import kotlin.math.abs
import kotlin.math.max

private val gridPadding = 4.dp
val rowHeight = 100.dp

@Composable
fun FolderScreen(
    currentFolder: String,
    onFolderChanged: (newFolder: String) -> Unit,
    onImageSelected: (ImageListSelection) -> Unit
) {
    val parentFolderList: List<Folder> = remember(currentFolder) {
        val parentFolder = File(currentFolder).parentFile
        if (parentFolder != null) {
            listOf(Folder(parentFolder, 0, 0))
        } else {
            emptyList()
        }
    }
    log("Folder: $currentFolder")
    val state = loadingStateFor(currentFolder) { loadFolderList(currentFolder) }
    when (state) {
        is LoadingState.Loading -> LoadingBox()
        is LoadingState.Success -> {
            val folderList = state.data.filter { it.file.isHidden.not() }.sortedBy { it.file.name }
            val folders = parentFolderList + folderList.filter { it.childFileCount < 1 }
            val mediaFolders = folderList.filter { it.childFileCount > 0 }
            log("Folder list: ${folderList.size}")
            LazyColumn(
                modifier = Modifier.fillMaxHeight(),
                verticalArrangement = Arrangement.spacedBy(-50.dp)
            ) {
                item {
                    Row(Modifier.height(100.dp)) {
                        folders.forEach { folder ->
                            Column(Modifier.clickable(onClick = {
                                onFolderChanged(folder.file.toString())
                            })) {
                                Image(
                                    modifier = Modifier.weight(1f).aspectRatio(1f),
                                    imageVector = Icons.Default.Folder,
                                    contentDescription = null
                                )
                                Text(folder.file.name)
                            }
                        }
                    }
                }
                items(mediaFolders) {
                    ImageRow(it, onImageSelected, onFolderChanged)
                }
            }
        }
        is LoadingState.Error -> {} // Show error message
        LoadingState.Start -> {} // Nothing to show
    }
}

@Composable
fun FolderItem(name: String, onClick: () -> Unit) {
    Column(Modifier.clickable(onClick = onClick)) {
        Image(
            modifier = Modifier.weight(1f).aspectRatio(1f),
            imageVector = Icons.Default.Folder,
            contentDescription = null
        )
        Text(name, modifier = Modifier.width(150.dp))
    }
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun ImageRow(
    folder: Folder,
    onImageSelected: (ImageListSelection) -> Unit,
    onFolderChanged: (newFolder: String) -> Unit
) {
    var isActive by remember { mutableStateOf(false) }
    val imageListState = loadingStateFor(folder) {
        loadImageListWithoutDimensions(folder.file)
    }
    val scaleFactor = animateFloatAsState(targetValue = if (isActive) 2f else 1f, animationSpec = tween(600))
    Row(Modifier.height(200.dp)
        .onPointerEvent(PointerEventType.Enter, onEvent = { isActive = true })
        .onPointerEvent(PointerEventType.Exit, onEvent = { isActive = false })
        .zIndex(if (isActive) 100f else 1f)
        .background(MaterialTheme.colors.primary.copy(alpha = 0.8f))
    ) {
        FolderItem(name = folder.file.name, onClick = { onFolderChanged(folder.file.toString()) })
        Spacer(Modifier.width(gridPadding))
        when (imageListState) {
            is LoadingState.Loading -> LoadingBox()
            is LoadingState.Success -> {
                //log("Item: ${imageListState.data}")
                HorizontalImageList(
                    modifier = Modifier.weight(1f),
                    isActiveRow = isActive,
                    imageList = imageListState.data,
                    onImageClicked = {
                        val selection =
                            ImageListSelection(imageListState.data, imageListState.data.indexOf(it))
                        onImageSelected(selection)
                    })
            }

            is LoadingState.Error -> {} //TODO show error
            LoadingState.Start -> {}
        }
    }
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun HorizontalImageList(
    modifier: Modifier,
    isActiveRow: Boolean,
    imageList: List<RemoteImage>,
    onImageClicked: (RemoteImage) -> Unit
) {
    val listState = rememberLazyListState()
    var isHover by remember { mutableStateOf(false) }
    var xScroll by remember { mutableStateOf(0f) }
    var xSize by remember { mutableStateOf(0f) }
    var xPos by remember { mutableStateOf(0f) }
    var xOffsetFromGlobal by remember { mutableStateOf(0f) }
    LaunchedEffect(isHover) {
        while (isActive && isHover) {
            if (xScroll < -0.1f || xScroll > 0.1f)
                listState.scrollBy(xScroll * 100f)
            delay(25)
        }
    }
    Box(modifier
        .onPlaced {
            xSize = it.size.width.toFloat()
            xOffsetFromGlobal = it.positionInRoot().x
        }
        .onPointerEvent(PointerEventType.Enter, onEvent = { isHover = true })
        .onPointerEvent(PointerEventType.Exit, onEvent = { isHover = false })
        .onPointerEvent(
            eventType = PointerEventType.Move,
            onEvent = {
                val scroll = it.changes.first().position
                val xRatio = scroll.x / xSize
                xScroll = xRatio - .5f
                xPos = scroll.x
                //println("$scroll xSize: $xSize ratio: $xScroll")
            })
    ) {
        val aspectRatio = 4f / 3f
        LazyRow(
            modifier = Modifier.fillMaxSize(),
            state = listState,
            horizontalArrangement = Arrangement.spacedBy((-128).dp)
        ) {
            items(imageList) { image ->
                var scale by remember { mutableStateOf(.5f) }
                var offset by remember { mutableStateOf(0f) }
                val itemModifier = if (isActiveRow) modifier.fillMaxSize().onPlaced {
                        val x = it.positionInRoot().x + it.size.width.toFloat() / 2f
                        val mouseX = xPos + xOffsetFromGlobal
                        val diff = (x - mouseX) / xSize
                        val tmp = 1f-abs(diff)
                        scale = max(.5f, (tmp * tmp)*1f)
                        //val dir = 1f
                        offset = diff//scaleX * it.size.width.toFloat()
                        //println("item pos: $x mouse pos: $xPos xDiff:${1f-diff} ")
                    }.scale(scale).zIndex(scale*10f).shadow(15.dp)//.scale(scaleX = offset, scaleY = 1f)
                else
                    Modifier.scale(.5f)

                Box(itemModifier
                    .fillMaxHeight()
                    .aspectRatio(aspectRatio)
                    .clickable { onImageClicked(image) }
                ) {
                    ThumbnailImage(
                        modifier = Modifier,
                        image = image,
                        overrideSize = 0
                    )
                    /*AnimatedVisibility(isActive,
                        enter = fadeIn(),
                        exit = fadeOut()
                    ) {
                        Box(
                            Modifier.align(Alignment.TopStart).fillMaxWidth()
                                .background(MaterialTheme.colors.primaryVariant.copy(alpha = 0.8f))
                        ) {
                            Text(image.file.name, Modifier.align(Alignment.Center))
                        }
                    }*/
                }
            }
        }
    }
}
