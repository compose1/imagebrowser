package imagebrowser

import androidx.compose.animation.Crossfade
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import de.appsonair.compose.*

/**
 * apps on air
 *
 * @author Timo Drick
 */
@Composable
fun ThumbnailImage(modifier: Modifier, image: RemoteImage, overrideSize: Int = 0) {
    var retryCounter: Int by mutableStateOf(0)
    BoxWithConstraints(modifier) {
        val width = if (overrideSize > 0) overrideSize else constraints.maxWidth
        val imageState = loadingStateFor(image) {
            loadImageSizeFileCached(image.file, width)
        }
        Crossfade(imageState) { state ->
            when (state) {
                is LoadingState.Loading -> LoadingBox()
                is LoadingState.Success -> Image(
                    modifier = Modifier.fillMaxSize(),
                    bitmap = state.data,
                    contentScale = ContentScale.Crop,
                    contentDescription = "image"
                )
                is LoadingState.Error -> ErrorBox(
                    message = "Error: ${state.error.message}",
                    onRetry = { retryCounter++ })
                LoadingState.Start -> {}
            }
        }
    }
}
