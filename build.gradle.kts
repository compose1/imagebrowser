import org.jetbrains.compose.compose
import org.jetbrains.compose.desktop.application.dsl.TargetFormat

plugins {
    id("com.github.ben-manes.versions") version "0.51.0"

    val kotlinVersion = "2.0.0"
    kotlin("jvm") version kotlinVersion
    kotlin("plugin.serialization") version kotlinVersion
    kotlin("plugin.compose") version kotlinVersion
    id("org.jetbrains.compose") version "1.6.11"
    //id("com.github.johnrengelman.shadow") version "6.1.0"
}

val group = "de.appsonair.compose"
val version = "1.0.0"

val startClass = "MainKt"
val appName = "ImageBrowser"

repositories {
    mavenCentral()
    maven { url = uri("https://maven.pkg.jetbrains.space/public/p/compose/dev") }
    google()
}

dependencies {
    implementation(compose.desktop.currentOs)
    implementation(compose.materialIconsExtended)

    implementation("uk.co.caprica:vlcj:4.8.3")

    val serialization = "1.7.1"
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-core:$serialization")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:$serialization")

    implementation("com.drewnoakes:metadata-extractor:2.19.0")
}

fun isNonStable(version: String): Boolean {
    val unStableKeyword = listOf("alpha", "beta", "rc", "cr", "m", "preview", "dev").any { version.contains(it, ignoreCase = true) }
    val regex = "^[0-9,.v-]+(-r)?$".toRegex()
    val isStable = unStableKeyword.not() || regex.matches(version)
    return isStable.not()
}
tasks.named("dependencyUpdates", com.github.benmanes.gradle.versions.updates.DependencyUpdatesTask::class.java).configure {
    rejectVersionIf {
        isNonStable(candidate.version)
    }
}


/*application {
    mainClassName = startClass
}*/
compose.desktop {
    application {
        mainClass = startClass
        nativeDistributions {
            targetFormats(
                TargetFormat.Dmg,
                TargetFormat.Msi,
                TargetFormat.Deb
            )
            packageName = appName
            packageVersion = "1.0.0"
            val iconsRoot = project.file("src/main/resources")
            linux {
                iconFile.set(iconsRoot.resolve("mango_icon.png"))
            }
        }
    }
}